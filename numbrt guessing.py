# This is guess the number game
import random

guessesTaken = 0

print('hello! what is your name?')
myname = input()

number = random.randint(1, 20)
print('well, ' + myname + '. I am thinking of a number between 1 and 20.')

while guessesTaken < 6:
    print('take a guess.')
    guess = input()
    guess = int(guess)

    guessesTaken = guessesTaken + 1

    if guess < number:
        print('your guess is to low.')

    if guess > number:
        print('your guess is too high')

    if guess == number:
        break

if guess == number:
    guessesTaken = str(guessesTaken)
    print('Good job, ' + myname + '! You guessed my number in ' + guessesTaken + ' guesses!')

if guess != number:
    number = str(number)
    print('Nope.the number I was thinking of was ' + number)
    
        
